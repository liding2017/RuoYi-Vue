package com.ruoyi.system.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.SystemParams;
import com.ruoyi.system.service.ISystemParamsService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 系统参数Controller
 * 
 * @author ruoyi
 * @date 2023-07-31
 */
@RestController
@RequestMapping("/system/params")
public class SystemParamsController extends BaseController
{
    @Autowired
    private ISystemParamsService systemParamsService;

    /**
     * 查询系统参数列表
     */
    @PreAuthorize("@ss.hasPermi('system:params:list')")
    @GetMapping("/list")
    public TableDataInfo list(SystemParams systemParams)
    {
        startPage();
        List<SystemParams> list = systemParamsService.selectSystemParamsList(systemParams);
        return getDataTable(list);
    }

    /**
     * 导出系统参数列表
     */
    @PreAuthorize("@ss.hasPermi('system:params:export')")
    @Log(title = "系统参数", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, SystemParams systemParams)
    {
        List<SystemParams> list = systemParamsService.selectSystemParamsList(systemParams);
        ExcelUtil<SystemParams> util = new ExcelUtil<SystemParams>(SystemParams.class);
        util.exportExcel(response, list, "系统参数数据");
    }

    /**
     * 获取系统参数详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:params:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(systemParamsService.selectSystemParamsById(id));
    }

    /**
     * 新增系统参数
     */
    @PreAuthorize("@ss.hasPermi('system:params:add')")
    @Log(title = "系统参数", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SystemParams systemParams)
    {
        return toAjax(systemParamsService.insertSystemParams(systemParams));
    }

    /**
     * 修改系统参数
     */
    @PreAuthorize("@ss.hasPermi('system:params:edit')")
    @Log(title = "系统参数", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SystemParams systemParams)
    {
        return toAjax(systemParamsService.updateSystemParams(systemParams));
    }

    /**
     * 删除系统参数
     */
    @PreAuthorize("@ss.hasPermi('system:params:remove')")
    @Log(title = "系统参数", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(systemParamsService.deleteSystemParamsByIds(ids));
    }
}
