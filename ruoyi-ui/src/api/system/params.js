import request from '@/utils/request'

// 查询系统参数列表
export function listParams(query) {
  return request({
    url: '/system/params/list',
    method: 'get',
    params: query
  })
}

// 查询系统参数详细
export function getParams(id) {
  return request({
    url: '/system/params/' + id,
    method: 'get'
  })
}

// 新增系统参数
export function addParams(data) {
  return request({
    url: '/system/params',
    method: 'post',
    data: data
  })
}

// 修改系统参数
export function updateParams(data) {
  return request({
    url: '/system/params',
    method: 'put',
    data: data
  })
}

// 删除系统参数
export function delParams(id) {
  return request({
    url: '/system/params/' + id,
    method: 'delete'
  })
}
