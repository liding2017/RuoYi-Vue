package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.SystemParams;

/**
 * 系统参数Service接口
 * 
 * @author ruoyi
 * @date 2023-07-31
 */
public interface ISystemParamsService 
{
    /**
     * 查询系统参数
     * 
     * @param id 系统参数主键
     * @return 系统参数
     */
    public SystemParams selectSystemParamsById(Long id);

    /**
     * 查询系统参数列表
     * 
     * @param systemParams 系统参数
     * @return 系统参数集合
     */
    public List<SystemParams> selectSystemParamsList(SystemParams systemParams);

    /**
     * 新增系统参数
     * 
     * @param systemParams 系统参数
     * @return 结果
     */
    public int insertSystemParams(SystemParams systemParams);

    /**
     * 修改系统参数
     * 
     * @param systemParams 系统参数
     * @return 结果
     */
    public int updateSystemParams(SystemParams systemParams);

    /**
     * 批量删除系统参数
     * 
     * @param ids 需要删除的系统参数主键集合
     * @return 结果
     */
    public int deleteSystemParamsByIds(Long[] ids);

    /**
     * 删除系统参数信息
     * 
     * @param id 系统参数主键
     * @return 结果
     */
    public int deleteSystemParamsById(Long id);
}
