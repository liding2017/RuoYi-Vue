package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.SystemParamsMapper;
import com.ruoyi.system.domain.SystemParams;
import com.ruoyi.system.service.ISystemParamsService;

/**
 * 系统参数Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-07-31
 */
@Service
public class SystemParamsServiceImpl implements ISystemParamsService 
{
    @Autowired
    private SystemParamsMapper systemParamsMapper;

    /**
     * 查询系统参数
     * 
     * @param id 系统参数主键
     * @return 系统参数
     */
    @Override
    public SystemParams selectSystemParamsById(Long id)
    {
        return systemParamsMapper.selectSystemParamsById(id);
    }

    /**
     * 查询系统参数列表
     * 
     * @param systemParams 系统参数
     * @return 系统参数
     */
    @Override
    public List<SystemParams> selectSystemParamsList(SystemParams systemParams)
    {
        return systemParamsMapper.selectSystemParamsList(systemParams);
    }

    /**
     * 新增系统参数
     * 
     * @param systemParams 系统参数
     * @return 结果
     */
    @Override
    public int insertSystemParams(SystemParams systemParams)
    {
        return systemParamsMapper.insertSystemParams(systemParams);
    }

    /**
     * 修改系统参数
     * 
     * @param systemParams 系统参数
     * @return 结果
     */
    @Override
    public int updateSystemParams(SystemParams systemParams)
    {
        return systemParamsMapper.updateSystemParams(systemParams);
    }

    /**
     * 批量删除系统参数
     * 
     * @param ids 需要删除的系统参数主键
     * @return 结果
     */
    @Override
    public int deleteSystemParamsByIds(Long[] ids)
    {
        return systemParamsMapper.deleteSystemParamsByIds(ids);
    }

    /**
     * 删除系统参数信息
     * 
     * @param id 系统参数主键
     * @return 结果
     */
    @Override
    public int deleteSystemParamsById(Long id)
    {
        return systemParamsMapper.deleteSystemParamsById(id);
    }
}
